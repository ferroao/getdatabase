
<!-- README.md is generated from README.Rmd. Please edit that file -->

<img src=logo.png align="right" width="12%">

# getDatabase<br></br>Repository to get data from public databases<br></br><br></br><br></br>

<!-- badges: start -->

<!-- badges: end -->

Common public databases include:

  - gbif
  - Open Tree of Life
  - Plant List
  - Plants of the World Online

### References

<div id="refs" class="references">

<div id="ref-R-data.table">

Dowle M, Srinivasan A. 2019. *Data.table: Extension of ‘data.frame‘*.
<https://CRAN.R-project.org/package=data.table> 

</div>

<div id="ref-R-stringi">

Gagolewski M, Tartanus B, IBM, Unicode, Inc., Unicode, Inc. 2019.
*Stringi: Character string processing facilities*.
<https://CRAN.R-project.org/package=stringi> 

</div>

<div id="ref-R-curl">

Ooms J. 2019. *Curl: A modern and flexible web client for r*.
<https://CRAN.R-project.org/package=curl> 

</div>

<div id="ref-R-ape">

Paradis E, Blomberg S, Bolker B, Brown J, Claude J, Cuong HS, Desper R,
Didier G, Durand B, Dutheil J, Ewing R, Gascuel O, Guillerme T, Heibl C,
Ives A, Jones B, Krah F, Lawson D, Lefort V, Legendre P, Lemon J, Marcon
E, McCloskey R, Nylander J, Opgen-Rhein R, Popescu A-A, Royer-Carenzi M,
Schliep K, Strimmer K, de Vienne D. 2019. *Ape: Analyses of
phylogenetics and evolution*. <https://CRAN.R-project.org/package=ape> 

</div>

<div id="ref-R-phytools">

Revell LJ. 2019. *Phytools: Phylogenetic tools for comparative biology
(and other things)*. <https://CRAN.R-project.org/package=phytools> 

</div>

<div id="ref-R-WriteXLS">

Schwartz M, Perl modules listed in each .pm file. 2019. *WriteXLS:
Cross-platform perl based r function to create excel 2003 (xls) and
excel 2007 (xlsx) files*. <https://CRAN.R-project.org/package=WriteXLS> 

</div>

<div id="ref-R-stringdist">

van der Loo M. 2019. *Stringdist: Approximate string matching and string
distance functions*. <https://CRAN.R-project.org/package=stringdist> 

</div>

<div id="ref-R-plyr">

Wickham H. 2016. *Plyr: Tools for splitting, applying and combining
data*. <https://CRAN.R-project.org/package=plyr> 

</div>

<div id="ref-R-reshape2">

Wickham H. 2017. *Reshape2: Flexibly reshape data: A reboot of the
reshape package*. <https://CRAN.R-project.org/package=reshape2> 

</div>

<div id="ref-R-assertthat">

Wickham H. 2019a. *Assertthat: Easy pre and post assertions*.
<https://CRAN.R-project.org/package=assertthat> 

</div>

<div id="ref-R-httr">

Wickham H. 2019b. *Httr: Tools for working with urls and http*.
<https://CRAN.R-project.org/package=httr> 

</div>

<div id="ref-R-stringr">

Wickham H. 2019c. *Stringr: Simple, consistent wrappers for common
string operations*. <https://CRAN.R-project.org/package=stringr> 

</div>

<div id="ref-R-readxl">

Wickham H, Bryan J. 2019. *Readxl: Read excel files*.
<https://CRAN.R-project.org/package=readxl> 

</div>

<div id="ref-R-dplyr">

Wickham H, François R, Henry L, Müller K. 2019. *Dplyr: A grammar of
data manipulation*. <https://CRAN.R-project.org/package=dplyr> 

</div>

<div id="ref-R-tidyr">

Wickham H, Henry L. 2019. *Tidyr: Easily tidy data with ’spread()’ and
’gather()’ functions*. <https://CRAN.R-project.org/package=tidyr> 

</div>

<div id="ref-R-hexSticker">

Yu G. 2019. *HexSticker: Create hexagon sticker in r*.
<https://CRAN.R-project.org/package=hexSticker> 

</div>

</div>
