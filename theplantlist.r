getwd()

# setwd("~/GoogleDrive/artigobaru/2017filo/genera")

library(data.table)
# read table csv
Raputable<-fread(list.files(pattern=".csv"), encoding = "Latin-1")
colnames(Raputable)
# create genus column
Raputable$genus<-sub("([[:alpha:]]+).*","\\1",Raputable$ESPECIE)

# eliminate duplicates
listofgeneraRaputpl<-unique(Raputable$genus)
length(listofgeneraRaputpl)  
116

listtosearch<-listofgeneraRaputpl
# build URLs
urls<-paste0("www.theplantlist.org/tpl1.1/search?q=",listtosearch,"&csv=true")
# urls<-paste0("www.theplantlist.org/tpl1.1/search?q=",listofgenerafabatpl$genus,"&csv=true")
urls[1]

# for (i in 1:length(listofgenerafabatpl$genus)){
getwd()

library(curl)
library(httr)
# sudo apt-get install libcurl4 libcurl4-openssl-dev -y
# install.packages("curl")

# listtosearch<-listofgenerafabatpl$genus

dir.create("csv_folder")
setwd(paste0(getwd(),"/csv_folder") ) 
getwd()
list.files() # see if already downloaded

#
# search URLs to download csv  DOWNLOADED, SEE ABOVE
#

# time 5 min
for (i in 1:length(listtosearch) ) {
    GET(urls[i], write_disk(paste0(listtosearch[i],".csv"), overwrite=TRUE))
}
# time 5 min

# read csvs
dataFiles <- lapply(Sys.glob("*.csv"), read.csv, stringsAsFactors=F) # strings as factors is critic

# build one dataframe from all csv
library(plyr)
tplRapudf <- ldply(dataFiles, data.frame)
colnames(tplRapudf)
head(tplRapudf)

# match original table with downloaded data in  a new column MATCH WITH GENUS COLUMN
Raputable$Family_TPL<-tplRapudf$Family[match(Raputable$genus,tplRapudf$Genus)]

unique(Raputable[which(is.na(Raputable$Family_TPL)),]$ESPECIE )
# [1] "DATOS NO DISPONIBLES"      "NO HAY"                    "?Egeria densa"             "?Ludwigia peploides"      
# [5] "?Ludwigia hexapetala"      "?Myriophyllum brasiliense" "?Robinia pseudacacia"      "?Solidago canadensis"     
# [9] "Myriophyltum spicatur"     "EgerIa densa"              "Glycerla fluitans"  

#############################################################################################################
# add column family to Raputable


# tplfabageneradf <- ldply(dataFiles, data.frame)

# head(tplfabageneradf)
# unique(tplfabageneradf$Infraspecific.rank)

clipboardp(unique(tplfabageneradf$Taxonomic.status.in.TPL))
# Synonym, Unresolved, Accepted, Misapplied
############################3
clipboardp<-function(x, sep="\t", row.names=FALSE, col.names=F){
  con <- pipe("xclip -selection clipboard -i", open="w")
  writeChar(paste(x, collapse = ", "), con)
  close(con) 
}

paste_noNA <- function(x,sep=", ") { gsub(", " ,sep, toString(x[!is.na(x) & x!="" & x!="NA"] ) ) }

unique(tplfabageneradf$Genus.hybrid.marker)
unique(tplfabageneradf$Species.hybrid.marker)

nrow(tplfabageneradf)#79693 79692 79507

# hybrid removal
tplfabageneradf<-tplfabageneradf[which(tplfabageneradf$Genus.hybrid.marker %ni% "×"),]
tplfabageneradf<-tplfabageneradf[which(tplfabageneradf$Species.hybrid.marker %ni% c("×","+") ),]

head(tplfabageneradf)

tplfabageneradf$namenoauthor <- apply( tplfabageneradf[ , c(5,7,8,9) ] , 1 , paste_noNA , sep=" ")
tplfabageneradf$nameandauthor <- apply( tplfabageneradf[ , c(5,7,8,9,10) ] , 1 , paste_noNA , sep=" ")

unique(tplfabageneradf$Taxonomic.status.in.TPL)

tplfabageneradfacce<-tplfabageneradf[which(tplfabageneradf$Taxonomic.status.in.TPL=="Accepted"),]
nrow(tplfabageneradfacce)#26858

# tplfabageneradf<-
head(tplfabageneradf[which(tplfabageneradf$Taxonomic.status.in.TPL=="Synonym"),] )
nrow(tplfabageneradf)#79507

tplfabageneradfNOTacce<-tplfabageneradf[which(tplfabageneradf$Taxonomic.status.in.TPL!="Accepted"),]
nrow(tplfabageneradfNOTacce)#26858 52649
head(tplfabageneradfNOTacce)#26858 52649
head(tplfabageneradf)
tplfabageneradfNOTacce$namenoauthoracce<-tplfabageneradf$namenoauthor[match(tplfabageneradfNOTacce$Accepted.ID,
                                                                                     tplfabageneradf$ID)]
tplfabageneradfNOTacce$nameandauthoracce<-tplfabageneradf$nameandauthor[match(tplfabageneradfNOTacce$Accepted.ID,
                                                                            tplfabageneradf$ID)]
# tplfabageneradfNOTacce$IDofacce<-tplfabageneradf$ID[match(tplfabageneradfNOTacce$Accepted.ID,
#                                                                               tplfabageneradf$ID)]

tail(tplfabageneradfNOTacce,30)#26858 52649



df















